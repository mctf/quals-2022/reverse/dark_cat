# Dark_cat

## Описание 
Дорога к поиску бубенчика привела нас в клуб с игровыми автоматами(примерно как сейчас есть в Японии) только всё в стиле Стимпанка. Помимо игровых автоматов там есть различные маленькие устройства, одно из них это стимпанковский Gameboy. Это место напоминает музей Яндекса со старой техникой

Игра начинается с того, как вы просыпаетесь рядом со своими братьями-котами в вашем логове. Один из них говорит главному герою:

“О, наконец-то ты проснулся. Ты ведь помнишь, что сегодня мы собираемся в длинное путешествия за пределы нашего дома?”

“ Конечно, дайте мне немного времени и можно отправляться. “

Итак, ваша первая задача это подойти к луже и попить воды. После вы отправляетесь к своей комнатке и на столе вас уже ждёт собранный рюкзак. Всё что остаётся, это надеть его.

Далее вы выходите из дома и видите своих братьев:

“Надеюсь, ты хорошо выспался. Дорога будет дальней.”
Вы начинаете идти и прыгаете по разным трубам, лестницам, траве и даже по перилам, в конечном итоге дорога приводит вас в таинственное… темное место. Вы доходите до возвышенности, на которую вам предстоит запрыгнуть. Первым идёт один из ваших братьев, и он с лёгкостью перепрыгивает обрыв. Дальше ваша очередь:

“Не бойся, здесь нет ничего сложного, просто повторяй за мной.”
Но как бы вы не прыгали, у вас не получится это сделать.

Я надеюсь, вы сможете помочь котофею! Мы верим в вас, друзья!

## Сложность 
Middle

## Writeup 
Для начала нам потребуется какой-то GB эмулятор, я в качестве примера использовал visual boy advance

Описание задачи нам как бы намекает (но возможно вам было плевать и вы не читали...), что нам нужно куда-то запрыгнуть и добраться до какой-то точки...  а что, если попытаться закинуть персонажа в самый конец карты?
Можно попробовать, но как нам это сделать? 

Для этого есть прекрасный инструмент под названием "Memory Viewer" (он, кстати, встроен в эмуляторы). 
Этот инструмент дает нам возможность просматривать динамически изменяющиеся участки памяти и менять их при своем желании. 

Для того чтобы попытаться как-то переместить нашего персонажа, мы должны найти те участки памяти, которые отвечают за координаты X и Y, довольно логично, правда?

Немного поперемещавшись по карте, мы замечаем, что у нас изменяются следующие значения: 
![image.png](./image.png)

Попробуем переназначить их значения на `FF 5F`:
![image-2.png](./image-2.png)

Вот мы и оказались на краю карты, теперь видим какого-то NPC... возможно нам стоит попасть именно к нему?
![image-3.png](./image-3.png)

Теперь давайте попробуем найти Y координаты, благо исходя из логики, они должны находится рядом с X координатами, что, собственно, мы и наблюдаем: 
![image-4.png](./image-4.png)

Попытаемся их поменять:
![image-5.png](./image-5.png)


А вот и наш флаг :)
![image-6.png](./image-6.png)
